<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Harvey Tour/Say</name>
        <message>
            <source>Let's go and see harvey</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's go and see harvey</translation>
        </message>
        <message>
            <source>time to see harvey</source>
            <comment>Text</comment>
            <translation type="obsolete">time to see harvey</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Harvey Tour/Say (1)</name>
        <message>
            <source>Now, lets see if we can get the real Harvey to pick a capsicum</source>
            <comment>Text</comment>
            <translation type="obsolete">Now, lets see if we can get the real Harvey to pick a capsicum</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Harvey Tour/Say (2)</name>
        <message>
            <source>Please note that the cutting tool is not active in this demo for safety reasons.</source>
            <comment>Text</comment>
            <translation type="obsolete">Please note that the cutting tool is not active in this demo for safety reasons.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Harvey Tour/Say (3)</name>
        <message>
            <source>This is embarassing. I seem to be a bit lost</source>
            <comment>Text</comment>
            <translation type="obsolete">This is embarassing. I seem to be a bit lost</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Harvey Tour/Say (4)</name>
        <message>
            <source>It seems that Harvey is not interested in playing today</source>
            <comment>Text</comment>
            <translation type="obsolete">It seems that Harvey is not interested in playing today</translation>
        </message>
    </context>
</TS>
