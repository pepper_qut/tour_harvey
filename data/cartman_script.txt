Let me tell you a story about my friend Cartman.
The task of identifying objects and picking them up is much more difficult for a robot than you might think. 
Robots just don't have the kind of dexterity that humans have.
But some of the best robotics groups in the world are working on the problem of grasping.
Big companies like Amazon really want to find a solution.
Human hands have 27 degrees of freedom. It's really amazing what you people can do with them.
Boy, I wish I could do that.
Last year, a team of researchers travelled to Nagoya in my home country of Japan to participate in an international competition called the Amazon Robotics Challenge.
It was a competition for robots which could identify different objects, pick them up, and then pack them into a box.
There were teams from around the world, competing for lots of prize money.
Everyone wanted to show how clever they were.
Some of the robots had really sophisticated hands that could do sensing and grasping.
The competition took place over four days, in front of a live audience. It was really intense.
Some of the teams were really professional. With sponsorship from major corporations.
Most robots either had claws, or some sort of suction system. 
Compared to lots of the other robots, Cartman is really quite simple.
He relies on having really good vision instead.
Our team didn't have a very good start to the competition when Cartman broke his hand on the first day.
But that is part of the challenge of taking a robot out of the lab and having it work in the real world.
After some emergency surgery, we were able to recover, and our team won the competition overall.
We think everyone was a bit surprised.
Maybe we were too.